from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import time

driver = webdriver.Chrome()

try:
    driver.get("https://www.google.com/gmail/about/")
    time.sleep(1)
    assert "Gmail: Private and secure email at no cost" in driver.title
    print("Gmail opened!")
    
    signInButton = driver.find_element(By. XPATH, "//a[normalize-space()='Sign in']").click()
    time.sleep(1)
    assert "Gmail" in driver.title
    print("Sign In Button clicked!")
    
    inputEmail =  driver.find_element(By. XPATH, "//input[@id='identifierId']").send_keys("ruthmeliora@gmail.com")
    time.sleep(1)
    print("Email has been inputted!")
    
    nextButton = driver.find_element(By.XPATH, "//div[@id='identifierNext']").click()
    print("Next Button clicked!")
    time.sleep(5)
    
    welcomeText = driver.find_element(By.XPATH, "//span[contains(text(), 'Couldn’t sign you in')]")    
    assert "sign you in" in welcomeText.text
    print("'Couldn't sign you in' text detected!")
    
except NoSuchElementException:
    print("Element isn't found.")
    time.sleep(1)
except AssertionError:
    print("Unexpected error!")
except Exception as e:
    print("An error occured: ", e)
    time.sleep(1)

finally:
    driver.quit