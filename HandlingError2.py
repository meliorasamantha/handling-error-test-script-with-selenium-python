from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import time

driver = webdriver.Chrome()

try:
    driver.get("https://www.google.com/gmail/about/")
    time.sleep(1)
    assert "Gmail: Private and secure email at no cost" in driver.title
    print("Gmail opened!")
    
    signInButton = driver.find_element(By.XPATH, "//a[normalize-space()='Sign in']").click()
    time.sleep(1)
    assert "Gmail" in driver.title
    print("Sign In Button clicked!")
    
    inputEmail = driver.find_element(By.XPATH, "//input[@id='identifierId']").send_keys("meliorasamantha@gmail.com")
    time.sleep(1)
    print("Email has been inputted!")
    
    nextButton = driver.find_element(By.XPATH, "//div[@id='identifierNext']").click()
    print("Next button clicked!")
    time.sleep(2)
    
    welcomeText = driver.find_element(By.XPATH, "//a[@aria-label='Try again']")
    assert "Try again" in welcomeText.text
    print("'Could't sign you in'")

except NoSuchElementException:
    print("No element detected!")

except AssertionError:
    print("Unexpected error!")
    time.sleep(1)
    
except Exception as e:
    print("An error occured: ", e)
    time.sleep(1)
finally:
    driver.quit
    