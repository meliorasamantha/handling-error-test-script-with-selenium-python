from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
import time

browser = webdriver.Chrome()
browser.get("https://www.btn.co.id/")
assert "Bank BTN Sahabat" in browser.title

"""
hover, button konverter, pindah page, scroll down, masukin input
"""
wait = WebDriverWait(browser, 20)
hoverD = wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@class='btn-valas-slider slick-initialized slick-slider slick-vertical']//div[@class='slick-list draggable']")))
hoverD.click()

clickBtn = wait.until(EC.element_to_be_clickable((By.XPATH, "//a[normalize-space()='e-kurs konverter kalkulator']")))
clickBtn.click()

assert "E-Konverter Hitungan Kurs" in browser.title
browser.execute_script("window, scrollBy(0,1500);")


inputMon = wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@id='SourceNumber']")))
inputMon.click()
inputMon.clear()
inputMon.send_keys("2")
time.sleep (5)

chooseCurrency = wait.until(EC.element_to_be_clickable((By.XPATH, "(//select[@name='CurrencyCode'])[2]")))
chooseCurrency.click()
time.sleep(2)
chooseCurrency = wait.until(EC.element_to_be_clickable((By.XPATH, "//option[@value='IDR']")))
chooseCurrency.click()
time.sleep(2)
hitBtn = wait.until(EC.element_to_be_clickable((By.XPATH, "(//input[@value='hitung']")))
hitBtn.click()
